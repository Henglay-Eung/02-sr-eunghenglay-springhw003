package com.hrd.homework.controller.controller;

import com.hrd.homework.controller.model.Article;
import com.hrd.homework.controller.repository.ArticleRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class AdminController {
    private ArticleRepository articleRepository;

    public AdminController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    //redirect to dashboard page
    @GetMapping("/admin")
    public String getAdminPage(){
        return "redirect:/admin/dashboard";
    }

    //view dashboard page
    @GetMapping("/admin/dashboard")
    public String getDashboardPage(){
        return "/admin/admin-dashboard";
    }

    //review articles page
    @GetMapping("/admin/review-articles")
    public String getReviewerPage(ModelMap modelMap){
        List<Article> articles = articleRepository.findAll();
        modelMap.addAttribute("articles",articles);
        return "/admin/article-list";
    }

    //add article page
    @GetMapping("/admin/article")
    public String getArticleManagementPage(@ModelAttribute Article article, ModelMap modelMap){
        modelMap.addAttribute("article",article);
        return "/admin/article-add";
    }

    //add article
    @PostMapping("/admin/article")
    public String postArticle(ModelMap modelMap, @ModelAttribute @Valid Article article, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "/admin/article-add";
        }
        articleRepository.save(article);
        modelMap.addAttribute("article",article);
        return "/admin/article-view";
    }

    //delete article
    @GetMapping("/admin/article/{id}")
    public String deleteArticle(@PathVariable int id){
        articleRepository.deleteById(id);
        return "redirect:/admin/review-articles";
    }

    //view an article page
    @GetMapping("/admin/article/view/{id}")
    public String viewArticle(@PathVariable int id,ModelMap modelMap){
        Optional<Article> article = articleRepository.findById(id);
        modelMap.addAttribute("article",article.get());
        return "/admin/article-view";
    }

    //update article page
    @GetMapping("/admin/article/update/{id}/")
    public String getArticleManagementPageForUpdate(@ModelAttribute Article article, ModelMap modelMap){
        Optional<Article> articleToUpdate = articleRepository.findById(article.getId());
        modelMap.addAttribute("article",articleToUpdate.get());
        return "/admin/article-update";
    }

    //update article
    @PostMapping("/admin/article/update/{id}")
    public String updateArticle(@ModelAttribute @Valid Article article,@PathVariable int id, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "/admin/article-update";
        }
        article.setId(id);
        articleRepository.save(article);
        return "/admin/article-view";
    }

    //view login page
    @GetMapping("/admin/login")
    public String login(){
        return "/admin/login";
    }

    //view logout page
    @GetMapping("/admin/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "/admin/logout";
    }

    //view homepage
    @GetMapping("/admin/home")
    public String getHome(){
        return "/admin/admin-home";
    }
}
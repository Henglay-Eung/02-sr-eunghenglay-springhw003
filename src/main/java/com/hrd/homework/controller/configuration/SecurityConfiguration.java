package com.hrd.homework.controller.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

       auth.inMemoryAuthentication()
               .withUser("dara").password("{noop}dara123").roles("USER").and()
               .withUser("kanha").password("{noop}kanha123").roles("USER","EDITOR").and()
               .withUser("reksmey").password("{noop}reksmey123").roles("USER","REVIEWER").and()
               .withUser("makara").password("{noop}makara123").roles( "USER","EDITOR","REVIEWER","ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/admin").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/admin/dashboard").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/admin/article").hasRole("EDITOR")
                .antMatchers(HttpMethod.GET, "/admin/review-articles").hasRole("REVIEWER")
                .antMatchers(HttpMethod.GET, "/admin/dashboard").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/admin/login")
                .permitAll()
                .defaultSuccessUrl("/admin/home",true);
    }
}
